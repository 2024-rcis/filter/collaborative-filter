FROM openjdk:17
# VOLUME [ "/tmp" ]
COPY target/collaborative-filter.jar collaborative-filter.jar
#COPY backend/src/main/resources/db/migrationScripts
ENTRYPOINT [ "java", "-jar", "/collaborative-filter.jar" ]
