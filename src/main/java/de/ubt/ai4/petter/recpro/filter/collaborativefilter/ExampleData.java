package de.ubt.ai4.petter.recpro.filter.collaborativefilter;

import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ExampleData {

    public static User getTargetUser() {
        return createUser("user3",null,3.0,3.0,1.0,1.0,null);
    }

    public static User createUser(String userId, Double item1, Double item2, Double item3, Double item4, Double item5, Double item6) {
        Map<CharSequence, Double> targetRatings = new HashMap<>();
        targetRatings.put("1", item1);
        targetRatings.put("2", item2);
        targetRatings.put("3", item3);
        targetRatings.put("4", item4);
        targetRatings.put("5", item5);
        targetRatings.put("6", item6);

        return new User(userId, targetRatings, 0.0, 0.0, 0.0);
    }

    public static List<User> getUsers() {
        List<User> result = new ArrayList<>();
        result.add(createUser("user1", 7.0, 6.0, 7.0, 4.0,5.0,4.0));
        result.add(createUser("user2", 6.0,7.0,null,4.0,3.0,4.0));
        result.add(createUser("user4", 1.0,2.0,2.0,3.0,3.0,4.0));
        result.add(createUser("user5",1.0,null,1.0,2.0,3.0,3.0));
        return result;
    }
}
