package de.ubt.ai4.petter.recpro.filter.collaborativefilter.base;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.filterpersistence.model.filter.FilterInstance;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/filter/collaborative/base")
@AllArgsConstructor
public class FilterController {
    private FilterService filterService;

    @GetMapping("/slopeOne")
    public void slopeOne() {
        filterService.slopeOneFilter();
    }

    @GetMapping("/cosine")
    public void cosine() {
        filterService.cosine();
    }

    @GetMapping("/pearson")
    public void pearson(@RequestParam int k, @RequestParam String itemId) {
        filterService.pearson(k, itemId);
    }

    @GetMapping("/userBasedCollaborativeFilteringWithCosineSimilarity")
    public ResponseEntity<Tasklist> userBasedCollaborativeFilteringWithCosineSimilarity(@RequestParam int k, @RequestHeader("X-User-ID") String userId, @RequestParam String filterId, @RequestParam Long tasklistId) {
        return ResponseEntity.ok(filterService.userBasedCollaborativeFilteringWithCosineSimilarity(k, userId, filterId, tasklistId));
    }

    @PostMapping("/userBased")
    public ResponseEntity<FilterInstance> userBased(@RequestBody FilterInstance filterInstance, @RequestParam int k) {
        return ResponseEntity.ok(filterService.userBased(filterInstance, k));
    }

}
