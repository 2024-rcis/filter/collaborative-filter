package de.ubt.ai4.petter.recpro.filter.collaborativefilter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Rating {

    private String userId;
    private String itemId;
    private double value;
}
