package de.ubt.ai4.petter.recpro.filter.collaborativefilter.model;

public enum SimilarityType {
    COSINE, PEARSON
}
