package de.ubt.ai4.petter.recpro.filter.collaborativefilter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String userId;
    private Map<CharSequence, Double> ratings = new HashMap<>();
    private double cosineSimilarity;
    private double pearsonCorrelation;
    private double meanRating;
}
