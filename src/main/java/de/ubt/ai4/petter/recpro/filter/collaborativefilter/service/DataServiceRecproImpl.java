package de.ubt.ai4.petter.recpro.filter.collaborativefilter.service;

import de.ubt.ai4.petter.recpro.filter.collaborativefilter.model.User;
import de.ubt.ai4.petter.recpro.filter.lib.recproapi.util.RecproDataService;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.IntervalBasedRatingInstance;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Profile("prod")
public class DataServiceRecproImpl implements IDataService {

    private RecproDataService dataService;
    @Override
    public Tasklist getTasklist(Long tasklistId) {
        return dataService.getTasklistById(tasklistId);
    }

    @Override
    public List<User> getUsers() {
        List<RatingInstance> ratings = dataService.getRatingInstances();
        Map<String, List<RatingInstance>> help = ratings.stream().collect(Collectors.groupingBy(RatingInstance::getUserId));
        Map<String, Map<CharSequence, Double>> result = new HashMap<>();

        List<User> users = new ArrayList<>();

        help.forEach((key, value) -> {
            User u = createUser(key, value);
            result.put(key, u.getRatings());
            users.add(u);
        });
        return users;
    }

    @Override
    public User getUserById(String userId) {
        return createUser(userId, getRatingsByUserId(userId));
    }

    private User createUser(String userId, List<RatingInstance> ratings) {
        User u = new User();
        u.setUserId(userId);
        Map<CharSequence, Double> ratingEntries = new HashMap<>();
        ratings.forEach(rating -> {
            if (rating instanceof IntervalBasedRatingInstance) {
                ratingEntries.put(rating.getRecproElementId(), Double.valueOf(((IntervalBasedRatingInstance) rating).getValue()));
            }
        });
        u.setRatings(ratingEntries);
        return u;
    }

    private List<RatingInstance> getRatingsByUserId(String userId) {
        List<String> userIds = new ArrayList<>();
        userIds.add(userId);
        return dataService.getByUserIds(userIds);
    }
}
