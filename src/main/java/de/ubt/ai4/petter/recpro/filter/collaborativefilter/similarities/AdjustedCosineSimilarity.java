package de.ubt.ai4.petter.recpro.filter.collaborativefilter.similarities;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class AdjustedCosineSimilarity {

    public Double similarity(final Map<CharSequence, Double> leftVector, final Map<CharSequence, Double> rightVector, double averageLeftRating, double averageRightRating) {
        if (leftVector == null || rightVector == null) {
            throw new IllegalArgumentException("Vectors must not be null");
        }

        final Set<CharSequence> intersection = getIntersection(leftVector, rightVector);

        final double dotProduct = dot(leftVector, rightVector, intersection, averageLeftRating, averageRightRating);

        double d1 = 0.0d;
        for (final Double value : leftVector.values()) {
            d1 += Math.pow((value), 2);
        }
        double d2 = 0.0d;
        for (final Double value : rightVector.values()) {
            d2 += Math.pow((value), 2);
        }
        double cosineSimilarity;
        if (d1 <= 0.0 || d2 <= 0.0) {
            cosineSimilarity = 0.0;
        } else {
            cosineSimilarity = (dotProduct / (Math.sqrt(d1) * Math.sqrt(d2)));
        }
        return cosineSimilarity;
    }

    /**
     * Returns a set with strings common to the two given maps.
     *
     * @param leftVector left vector map
     * @param rightVector right vector map
     * @return common strings
     */
    private Set<CharSequence> getIntersection(final Map<CharSequence, Double> leftVector,
                                              final Map<CharSequence, Double> rightVector) {
        final Set<CharSequence> intersection = new HashSet<>(leftVector.keySet());
        intersection.retainAll(rightVector.keySet());
        return intersection;
    }

    /**
     * Computes the dot product of two vectors. It ignores remaining elements. It means
     * that if a vector is longer than other, then a smaller part of it will be used to compute
     * the dot product.
     *
     * @param leftVector left vector
     * @param rightVector right vector
     * @param intersection common elements
     * @return the dot product
     */
    private double dot(final Map<CharSequence, Double> leftVector, final Map<CharSequence, Double> rightVector,
                       final Set<CharSequence> intersection, double averageLeftRating, double averageRightRating) {
        long dotProduct = 0;
        for (final CharSequence key : intersection) {
            dotProduct += (leftVector.get(key)) * (rightVector.get(key));
        }
        return dotProduct;
    }

}
